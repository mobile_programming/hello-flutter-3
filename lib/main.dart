import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() =>    _MyStatefulWidgetState();

}
String engGreeting = "Hello Flutter";
String spainGreeting = "Hola Flutter";
String thGreeting = "สวัสดี Flutter";
String chinaGreeting = "Nihao Flutter";

class  _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = engGreeting;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == engGreeting?
                    spainGreeting : engGreeting;
                  });
                },
                icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == engGreeting?
                    thGreeting : engGreeting;
                  });
                },
                icon: Icon(Icons.traffic)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == engGreeting?
                    chinaGreeting : engGreeting;
                  });
                },
                icon: Icon(Icons.abc)),
          ],

        ) ,
        body: Center(
          child: Text(displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }

}